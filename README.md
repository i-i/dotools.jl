# DoTools.jl

### Simple Linux Automation Wrapper

Intended for dumb desktop environment automation regardless of whether you're using X11 or Wayland.
Allows you to call xdotool or ydotool from within Julia. Supports a mutual subset of functionality of both utilities. Requires that the respective tool be installed or within the runtime path.

Note: for Wayland, it's recommended to run ydotoold seperately. It may be noted that ydotool works on X11 if ydotoold is running, but xdotool requires no external programs.

This library incomplete as of now, so much functionality is missing.

#### Examples of usage:

**XKCD's ["Command Line Fu"](https://xkcd.com/196/)**

Jiggles the pointer down 10 pixels and back up every minute for 90 minutes and then clicks. Intended to avert overzealous power saving as described in the comic.

```
using DoTools

for minute in 1:90
    mouseMove(0,10)
    sleep(1)
    mouseMove(0,-10)
    sleep(59)
end
click(1)
```

**Slow Paste**

* Types the contents of the clipboard after 2 seconds. Mimics typing.

```
import DoTools

ctx = DoTools.getContext()
sleep(2)
clipboard = ctx == DoTools.X11Context ? read(`xsel -bo`, String) : read(`wl-paste`, String)
DoTools.typeText(ctx, clipboard)
```
