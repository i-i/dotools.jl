module DoTools

abstract type DoToolContext end
struct X11Context <: DoToolContext end
struct WaylandContext <: DoToolContext end

getContext() = ENV["XDG_SESSION_TYPE"] == "x11" ? X11Context : WaylandContext

toolName(::Type{X11Context}) = "xdotool"
toolName(::Type{WaylandContext}) = "ydotool"

function typeText(ctx::Type{X11Context}, s::String, delay::Int=12)
    tn = [toolName(ctx), "type"]
    ds = delay==12 ? "" : ["--delay", "$delay"]
    run(`$tn $ds $s`)
end
function typeText(ctx::Type{WaylandContext}, s::String, delay::Int=12)
    tn = [toolName(ctx), "type"]
    ds = delay==12 ? "" : ["--key-delay", "$delay"]
    run(`$tn $ds $s`)
end
function typeText(s::String, delay::Int=12)
    ctx = getContext()
    typeText(ctx, s, delay)
end

function mouseMove(ctx::Type{X11Context}, x::Int, y::Int)
    tn = [toolName(ctx), "mousemove_relative"]
    run(`$tn -- $x $y`)
end
function mouseMove(ctx::Type{WaylandContext}, x::Int, y::Int)
    tn = [toolName(ctx), "mousemove"]
    run(`$tn -- $x $y`)
end
function mouseMove(x::Int, y::Int)
    ctx = getContext()
    mouseMove(ctx,x,y)
end

# TODO: Button mappings are not consistent between contexts 
function click(ctx::Type{X11Context},button::Int)
    tn = [toolName(ctx), "click"]
    run(`$tn $button`)
end
function click(ctx::Type{WaylandContext},button::Int)
    tn = [toolName(ctx), "click"]
    run(`$tn $button`)
end
function click(button::Int)
    ctx = getContext()
    click(ctx, button)
end

end # module
